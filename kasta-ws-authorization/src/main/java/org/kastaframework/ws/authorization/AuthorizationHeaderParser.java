package org.kastaframework.ws.authorization;

import java.util.Map;

/**
 * Parser for authorization header.
 *
 * @author Sabri Onur Tuzun
 * @since 11/1/14 9:48 PM
 */
public interface AuthorizationHeaderParser {

    /**
     * Parses authorization header string and returns parameters as map.
     *
     * @param authorizationStr authorization header string
     * @return parameter map
     */
    Map<String, String> parseAuthorizationString(String authorizationStr);
}
