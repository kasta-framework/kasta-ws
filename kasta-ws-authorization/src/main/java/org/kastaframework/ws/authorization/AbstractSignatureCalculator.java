package org.kastaframework.ws.authorization;

import org.apache.commons.lang3.StringUtils;
import org.kastaframework.io.digest.EncodingStrategyFactory;
import org.kastaframework.io.digest.EncodingStrategyType;

/**
 * Abstract base signature calculator.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 12:46 PM
 */
public abstract class AbstractSignatureCalculator implements SignatureCalculator {

    private EncodingStrategyFactory encodingStrategyFactory;

    /**
     * Constructor with encoding strategy factory.
     *
     * @param encodingStrategyFactory encodingStrategyFactory
     */
    protected AbstractSignatureCalculator(EncodingStrategyFactory encodingStrategyFactory) {
        this.encodingStrategyFactory = encodingStrategyFactory;
    }

    /**
     * Joins values and encodes appended strings with given encoding strategy.
     *
     * @param type             encoding strategy type
     * @param appendableValues appendable values
     * @return signature
     */
    @Override
    public String getSignature(EncodingStrategyType type, Object... appendableValues) {
        return encodingStrategyFactory.encode(type, StringUtils.join(appendableValues));
    }

    /**
     * Checks if given signatures are equal.
     *
     * @param signature           signature
     * @param calculatedSignature calculated signature
     * @return true if equals
     */
    @Override
    public boolean checkSignature(String signature, String calculatedSignature) {
        return StringUtils.equals(signature, calculatedSignature);
    }
}
