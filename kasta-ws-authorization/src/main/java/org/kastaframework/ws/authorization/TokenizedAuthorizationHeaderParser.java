package org.kastaframework.ws.authorization;

import org.apache.commons.lang3.StringUtils;
import org.kastaframework.collections.ArrayHelper;

import java.util.Map;

/**
 * Pre-defined splitted authorization header parser.
 *
 * @author Sabri Onur Tuzun
 * @since 11/1/14 9:07 PM
 */
public class TokenizedAuthorizationHeaderParser implements AuthorizationHeaderParser {

    private String authorizationHeaderName;
    private String parameterSeparator;
    private String equalitySeparator;

    /**
     * Default constructor.
     *
     * @param authorizationHeaderName authorization header name
     * @param parameterSeparator      parameter seperator
     * @param equalitySeparator       equality parser
     */
    public TokenizedAuthorizationHeaderParser(String authorizationHeaderName, String parameterSeparator, String equalitySeparator) {
        this.authorizationHeaderName = authorizationHeaderName;
        this.parameterSeparator = parameterSeparator;
        this.equalitySeparator = equalitySeparator;
    }

    /**
     * Parses authorization header for the pre-defined formats.
     *
     * @param authorizationStr authorization header string
     * @return parameter map
     */
    public Map<String, String> parseAuthorizationString(String authorizationStr) {
        String croppedStr = StringUtils.substring(authorizationStr, authorizationHeaderName.length() + 1);
        String[] parameters = StringUtils.split(croppedStr, parameterSeparator);
        return ArrayHelper.splitAndMap(parameters, equalitySeparator);
    }
}
