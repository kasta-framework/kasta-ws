package org.kastaframework.ws.authorization;

import org.kastaframework.io.digest.EncodingStrategyType;

/**
 * Signature calculator.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 11:11 AM
 */
public interface SignatureCalculator {

    /**
     * Prepares signature.
     *
     * @param type             encoding strategy type
     * @param appendableValues appendable values
     * @return signature
     */
    String getSignature(EncodingStrategyType type, Object... appendableValues);

    /**
     * Checks if given signatures are equal.
     *
     * @param signature           signature
     * @param calculatedSignature calculated signature
     * @return true if equals
     */
    boolean checkSignature(String signature, String calculatedSignature);
}
