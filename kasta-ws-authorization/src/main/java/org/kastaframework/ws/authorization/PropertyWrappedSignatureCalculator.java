package org.kastaframework.ws.authorization;

import org.kastaframework.io.digest.EncodingStrategyFactory;
import org.kastaframework.io.digest.EncodingStrategyType;

/**
 * Property wrapped object based signature calculator.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 12:42 PM
 */
public abstract class PropertyWrappedSignatureCalculator<T extends WsCredential> extends AbstractSignatureCalculator implements SignatureCalculator {

    /**
     * Constructor with encoding strategy factory.
     *
     * @param encodingStrategyFactory encodingStrategyFactory
     */
    protected PropertyWrappedSignatureCalculator(EncodingStrategyFactory encodingStrategyFactory) {
        super(encodingStrategyFactory);
    }

    /**
     * Calculates signature of given object.
     *
     * @param type       encoding strategy type
     * @param credential credential
     * @return signature
     */
    public abstract String getSignatureOfObject(EncodingStrategyType type, T credential);

    /**
     * Calculates and checks signature of given object.
     *
     * @param signature  signature
     * @param type       encoding strategy type
     * @param credential credential
     * @return true is equals
     */
    public boolean checkSignatureOfObject(String signature, EncodingStrategyType type, T credential) {
        String calculatedString = getSignatureOfObject(type, credential);
        return checkSignature(signature, calculatedString);
    }
}
