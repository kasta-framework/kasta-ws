package org.kastaframework.ws.authorization;

/**
 * Marker interface for web service authorize operation.
 *
 * @author Sabri Onur Tuzun
 * @since 11/3/14 1:24 PM
 */
public interface WsCredential {
}
